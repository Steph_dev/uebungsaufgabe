package de.stephan.uebungsaufgabe.service;

import de.stephan.uebungsaufgabe.model.Measurement;
import de.stephan.uebungsaufgabe.model.MeasurementType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MeasurementServiceTest {
    @Autowired
    MeasurementService measurementService;

    @Test
    void sample() {
        List<Measurement> testList = getTestList();
        Map<MeasurementType, List<Measurement>> returnMap = measurementService.sample(testList);
        assertEquals(3, returnMap.get(MeasurementType.TEMP).size());
        assertEquals(2, returnMap.get(MeasurementType.SPO2).size());
        for (Map.Entry<MeasurementType, List<Measurement>> entry : returnMap.entrySet()) {
            MeasurementType key = entry.getKey();
            List<Measurement> value = entry.getValue();
            System.out.println("Key: " + key);
            for (Measurement obj : value) {

                System.out.println("Value: " + obj.getMeasurementValue() + " Time:" + obj.getMeasurementTime());
            }

        }
    }

    private List<Measurement> getTestList() {
        List<Measurement> testList = new ArrayList<>();
        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 3, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(20)
                .measurementType(MeasurementType.SPO2)
                .build());

        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 7, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(30)
                .measurementType(MeasurementType.TEMP)
                .build());
        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 59, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(50)
                .measurementType(MeasurementType.TEMP)
                .build());

        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 3, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(10)
                .measurementType(MeasurementType.TEMP)
                .build());

        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 5, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(20)
                .measurementType(MeasurementType.TEMP)
                .build());


        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 6, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(20)
                .measurementType(MeasurementType.SPO2)
                .build());


        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 3, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(20.5)
                .measurementType(MeasurementType.SPO2)
                .build());
        testList.add(Measurement.builder()
                .measurementTime(ZonedDateTime.of(2023, 1, 1, 10, 7, 0, 0, ZoneId.systemDefault()).toInstant())
                .measurementValue(30)
                .measurementType(MeasurementType.SPO2)
                .build());

        return testList;
    }
}