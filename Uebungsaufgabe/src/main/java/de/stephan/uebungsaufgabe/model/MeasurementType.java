package de.stephan.uebungsaufgabe.model;

public enum MeasurementType {
    TEMP("Temperatur"),
    SPO2("SPO2");

    MeasurementType(String label) {
    }
}
