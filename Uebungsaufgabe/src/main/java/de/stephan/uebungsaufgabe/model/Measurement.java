package de.stephan.uebungsaufgabe.model;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class Measurement {

    private Instant measurementTime;
    private double measurementValue;

    private MeasurementType measurementType;
}
