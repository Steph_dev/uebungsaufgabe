package de.stephan.uebungsaufgabe.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class LocalTimeIntervall {
    LocalTime start;
    LocalTime end;
    Measurement measurement;
}
