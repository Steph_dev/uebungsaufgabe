package de.stephan.uebungsaufgabe.service;

import de.stephan.uebungsaufgabe.model.LocalTimeIntervall;
import de.stephan.uebungsaufgabe.model.Measurement;
import de.stephan.uebungsaufgabe.model.MeasurementType;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

@Service
@Log4j2
public class MeasurementService {
    public static final Comparator<Measurement> COMPARATOR = Comparator.comparing(Measurement::getMeasurementTime);
    public static final ZoneId ZONE_ID = ZoneId.systemDefault();
    int timeWindow = 5;
    Map<MeasurementType, List<Measurement>> returnMap = new HashMap<>();


    // nimmt Werte entgegen und kontrolliert ob Key bereits vorhanden, wenn nicht, wird ein neuer Key mit Wert erstellt, falls key bereits vorhanden, wird der neue Wert in die Map eingetragen bzw. ignjoriert (falls zeitlich nach dem aktuellen Wert liegt)
    public Map<MeasurementType, List<Measurement>> sample(
            List<Measurement> unsampledMeasurements) {
        for (Measurement measurementListObj : unsampledMeasurements) {
            MeasurementType measurementType = measurementListObj.getMeasurementType();
            List<Measurement> measurementList = returnMap.get(measurementType);
            if (measurementList == null) {
                List<Measurement> returnList = new ArrayList<>();
                returnList.add(measurementListObj);
                returnMap.put(measurementType, returnList);
            } else {
                // Fall: falls bereits ein Key in der Map vorhanden ist
                addToMap(measurementListObj);
            }
            //sortieren der Map nach Zeit
            returnMap.get(measurementType).sort(COMPARATOR);
        }
        return returnMap;
    }

    private void addToMap(Measurement measurement) {
        // bestimmen des Intervalls
        LocalTimeIntervall localTimeIntervall = getStartInterval(measurement);
        if (localTimeIntervall != null) {
            //falls intervall vorhanden, Kontrolle ob Wert relevant
            updateMap(localTimeIntervall);
        } else {
            log.error("Konnte kein Zeitintervall bestimmt werden!.");
        }
    }

    private void updateMap(LocalTimeIntervall localTimeIntervall) {
        Measurement measurement = localTimeIntervall.getMeasurement();
        MeasurementType measurementType = measurement.getMeasurementType();
        List<Measurement> measurementList = returnMap.get(measurementType);
        LocalTime start = localTimeIntervall.getStart();
        LocalTime end = localTimeIntervall.getEnd();
        LocalTime time = LocalTime.ofInstant(measurement.getMeasurementTime(), ZONE_ID);

        // map durchgehen ob ein Wert im zeitfenster liegt
        for (Measurement measurementListObj : measurementList) {
            LocalTime measurementListObjTime = LocalTime.ofInstant(measurementListObj.getMeasurementTime(), ZONE_ID);
            // Fall: falls gleiche zeit, wird neue zeit ignoriert
            if (measurementListObjTime.equals(time)) {
                return;
            }
            //Fall: liegt im Zeitfenster (start-end) und ist aktueller als vorhandener Wert
            if (measurementListObjTime.isAfter(start) && (measurementListObjTime.isBefore(end) || measurementListObjTime.equals(end)) && time.isAfter(measurementListObjTime)) {
                returnMap.get(measurementType).remove(measurementListObj);
                returnMap.get(measurementType).add(measurement);
                return;
                //Fall: liegt im Zeitfenster(start-end) aber ist nicht aktueller als vorhandener Wert
            } else if (measurementListObjTime.isAfter(start) && (measurementListObjTime.isBefore(end) || measurementListObjTime.equals(end)) && time.isBefore(measurementListObjTime)) {
                return;
            }
        }
        // Fall: kein Wert im Zeitfenster gefunden, also hinzufügen als aktueller Wert
        returnMap.get(measurementType).add(measurement);
    }

    private LocalTimeIntervall getStartInterval(Measurement measurement) {
        LocalTime time = LocalTime.ofInstant(measurement.getMeasurementTime(), ZONE_ID);
        LocalTime start = LocalTime.of(time.getHour(), 0);
        LocalTime end = LocalTime.of(time.getHour(), timeWindow);
        int iterator = 1;
        int maxIterations = 60 / timeWindow;
        while (iterator <= maxIterations) {
            if (time.isAfter(start) && time.isBefore(end) || time.equals(end)) {
                return LocalTimeIntervall.builder()
                        .start(start)
                        .end(end)
                        .measurement(measurement)
                        .build();
            }
            start = start.plusMinutes(timeWindow);
            end = end.plusMinutes(timeWindow);
            iterator += 1;
        }
        return null;
    }

}
