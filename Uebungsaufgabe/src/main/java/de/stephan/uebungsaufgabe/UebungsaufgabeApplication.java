package de.stephan.uebungsaufgabe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UebungsaufgabeApplication {

    public static void main(String[] args) {
        SpringApplication.run(UebungsaufgabeApplication.class, args);
    }

}
