from collections import defaultdict
from datetime import datetime, timedelta

class Measurement:
    def __init__(self, measurementType, instant, value):
        self.measurementType = MeasurementType(measurementType)
        self.instant = instant
        self.value = value

class MeasurementType:
    def __init__(self, value):
        if value not in ['SPO2', 'TEMP']:
            raise ValueError("Invalid measurement type")
        self.value = value

# Runde Zeit auf (immer auf nächsten 5 Minuten Wert)
def round_timestamp(timestamp):
    # rundet Zeitwert ab, außer ist Vielfaches von 5
    rounded_minutes = (timestamp.minute // 5) * 5
    rounded_time = timestamp.replace(minute=rounded_minutes, second=0, microsecond=0)
    # wenn timestamp kein vielfaches von 5 ist addiere +5 Minuten, da vorher abgerundet wurde
    if timestamp.minute % 5 != 0:
        rounded_time += timedelta(minutes=5)
    return rounded_time

def convert_to_map(measurement_list):
    measurement_map = {}
    #
    for measurement in measurement_list:
        measurement_type = measurement.measurementType.value
        rounded_instant = round_timestamp(measurement.instant)
        # verwende 2 keys, den measurement_type und die aufgerundete Zeit, dadurch kann es nur einen Eintrag für das zeitintervall geben
        key = (measurement_type, rounded_instant)
        # fügt der map hinzu, wenn noch kein keypar vorhanden oder wenn der neue Wert aktueller ist, im Fall, dass der gleiche zeitstempel vorkommt, wird der 2. Ignoriert
        if key not in measurement_map or measurement.instant > measurement_map[key].instant:
            measurement_map[key] = measurement
            # sortiere die Map nach dem Ersten und dann nach dem Zweiten Key 
    return dict(sorted(measurement_map.items(), key=lambda x: (x[0][0], x[0][1])))
    

# Beispiel Measurement-Liste
measurement_list = [
    Measurement("TEMP", datetime(2023, 6, 12, 10, 59, 0), 50),
    Measurement("TEMP", datetime(2023, 6, 12, 10, 2, 0), 3),
    Measurement("TEMP", datetime(2023, 6, 12, 10, 5, 0), 20),
    Measurement("TEMP", datetime(2023, 6, 12, 10, 5, 0), 30),
    Measurement("TEMP", datetime(2023, 6, 12, 23, 56, 0), 30),

    Measurement("SPO2", datetime(2023, 6, 12, 10, 0, 0), 98),
    Measurement("SPO2", datetime(2023, 6, 12, 10, 8, 0), 99.5),
    Measurement("TEMP", datetime(2023, 6, 12, 10, 4, 0), 10),
    Measurement("SPO2", datetime(2023, 6, 12, 10, 6, 0), 99),
    #Test Fehlerfall, falscher Measurementtype
    # Measurement("SP2", datetime(2023, 6, 12, 10, 6, 0), 99),

]

# Konvertierung der Measurement-Liste in eine Map
measurement_map = convert_to_map(measurement_list)

# Ausgabe der Measurement-Map
for measurement_type, measurement in measurement_map.items():
    print(measurement_type)
    print(f"Instant: {measurement.instant}, Value: {measurement.value}")
    print()
